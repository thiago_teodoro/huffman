/*! \mainpage Compactador Huffman 
  
  Esse é um pequeno programa que implementa o algoritmo de compactação
  de Huffman da forma como foi descrita no livro Introduction to Algorithms(3º edição,Thomas H. Cormen). 

  \section compile_sec Compilando
  Simplesmente execute <B>make</B>.

  \section doc_sec Gerando essa documentação
  Execute <B>make doc</B>.
  
  \section use_sec Como usar
  Basta executar <B>huffman --help</B> para obter a lista de opções.

  \section comp_sec Comparação com ZIP,RAR e TAR.
  
  Foi feita algumas comparações usando o arquivo Doxyfile utilizado para
  gerar essa documentação.
  O tamanho original era 108K (sáida de <B>du -hs Doxyfile</B>).
  Após a compactação com o GNU gzip, o novo tamanho foi de 28K.
  Com a utilização do RAR, o novo tamanho também foi de 28K.
  O arquivo Doxyfile.tar resultante do comando <B>tar cfv Doxyfile.tar Doxyfile</B>
  tem tamanho 112K. Por outro lado, o arquivo Doxyfile.tar.gz resultante do comando
  <B>tar cfvz Doxyfile.tar.gz Doxyfile</B> tem tamanho 28K. Finalmente, o arquivo
  Doxyfile.hf resultante do comando <B>huffman --compress Doxyfile --output Doxyfile.hf</B> possui tamanho 
  de 68K.
*/

/*! 
  \file main.c
  \brief Arquivo onde a compactação e descompactação é realizada.
  
  Aqui é realizado os processos de  compactação e descompactação, dependendo
  dos parâmetros passados para o programa. Veja \ref uso ou execute huffman --help 
  para uma lista de opções.
  
  O arquivo compactado inicia com um inteiro <B>N</B> indicando a quantidade de nós que a 
  árvore binária de codificação possui. Em seguida vem <B>N</B> linhas com quatro inteiros 
  indicando,respectivamente, o índice do nó, o código ascii do caractere que ele representa,
  o índice do filho da esquerda(-1 caso o nó não tenha filho na esquerda) e o índice do filho da direita
  (-1 caso o nó não tenha filho na direita). Após isso segue o texto codificado.

  O último byte do arquivo é a quantidade de bits do penúltimo byte que deve ser processada pela
  etapa de decodificação. Isso é necessário já que só podemos gravar/ler as informações em múltiplos 
  de 8 bits.
  
  
  */

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <getopt.h>
#include "heap.h"

/*! Tamanho máximo do nosso alfabeto. Como estamos usando a tabela ascii 
  então esse tamanho é 256  */
#define MAX_CHAR_SET_SIZE 256 	

/*! Essa é a nossa relação de ordem < para a estrutura \ref no. */
bool less_than(no * a,no * b)
{
  return a->freq < b->freq;
}

/*! Esse arranjo guarda as frequências de todos
  os possíveis 256 caracteres encontrados no 
  arquivo de entrada.  */
int _frequencias[MAX_CHAR_SET_SIZE];
/*! Ao final da execução do algoritmo de huffman, para cada caractere encontrado
  no arquivo de entrada haverá um ponteiro para a sua folha nesse arranjo.
  Exemplo: Suponha que tenhamos um arquivo cujo conteúdo é apenas abcd. Após 
  a execução do algoritmo de huffman teremos uma árvore cujas folhas correspondem
  aos caracteres a,b,c,d. Se quisermos acessar diretamente a folha que corresponde 
  ao caractere 'a' fazemos folhas['a']. O próprio código ascii desse caractere é
  o índice de sua folha no arranjo.
*/

no * _folhas[MAX_CHAR_SET_SIZE];

/*! A quantidade de caracteres encontrados no arquivo de entrada. */
int _char_set_size;

/*! \brief É essa função que gera a árvore binária correspondente a codificação utilizada.
  
  Inicialmente montamos uma fila de prioridade mínima com as _char_set_size folhas da árvore, que 
  correspondem aos caracteres encontrados no arquivo de entrada. Em seguida realizamos
  _char_set_size - 1 operações que consistem em alocar um novo nó z cujos filhos serão 
  os dois nós de menor frequência da fila de prioridade e cuja frequencia é a soma daquelas possuídas pelos filhos.
  A etapa final dessas operações é adicionar o novo nó z à fila. No final do algoritmo o único
  nó restante na fila é a própria raiz da árvore. Para detalhes do algoritmo de huffman consulte
  o livro Algoritmos e Estruturas de Dados do Cormen.
 
  \return  A raiz da árvore.
*/

no * huffman()
{


  heap * h = new_heap(less_than);

  int indice = 0;
  _char_set_size = 0;
  for (int i = 0; i < MAX_CHAR_SET_SIZE; i++)
    if (_frequencias[i] > 0)
      {
	no * n = malloc(sizeof(no));
	assert(n);

	n->c = (uint8_t)i;
	n->freq = _frequencias[i];
	n->l = n->r = n->p = NULL;
	n->indice = indice++;

	/* Guardo cada folha no arranjo _folhas, cujo indice corresponde ao caracter da folha. */
	_folhas[i] = n;
	min_heap_insert(h, n);

	_char_set_size++;
      }


  for (int i = 1; i <=  _char_set_size - 1; i++)
    {
      	no * z = malloc(sizeof(no));
	assert(z);

	no * x = heap_extract_min(h);
	no * y = heap_extract_min(h);
	
	z->c = 0;
	z->l = x;
	z->r = y;
	z->freq = x->freq + y->freq;
	z->p = NULL;
	z->indice = indice++;
	
	
	x->p = y->p = z;
	
	min_heap_insert(h, z);
    }


  no * r = heap_extract_min(h);
  
  free_heap(h);
  
  return r;
}

/*! Desalocamos recursivamente a árvore cuja raiz é \p root. */
void free_tree(no * root)
{
  if (root->l)
    free_tree(root->l);

  if (root->r)
    free_tree(root->r);

  free(root);
}


/*! Essa estrutura representa as informações do nó.  */
typedef struct __attribute__((__packed__)) info_no
{
  uint16_t indice;		/*!< O indice do nó.*/
  uint8_t c;			/*!< O caractere que ele representa.  */
  uint16_t il;           	/*!< O índice do filho da esquerda.  */
  uint16_t ir;		        /*!< O índice do filho da direita.  */
}info_no;


/*! Usamos uma recursão para escrever as informações dos nós da árvore, 
  segundo o cabeçalho na descrição do arquivo \ref main.c.
  
  \param out Arquivo onde será gravado essas informações.
  \param n O nó atual sendo processado.

  Basta passar para o parâmetro \p n a raíz da árvore para que ela seja totalmente gravada
  no arquivo \p out.
  */

void escrever_filhos_por_dfs(FILE * out,no * n)
{
  info_no in;

  in.indice = n->indice;
  in.c = n->c;
  
  if (n->l && n->r)
    {
      in.il = n->l->indice;
      in.ir = n->r->indice;

      fwrite(&in, 1, sizeof(in), out);
      escrever_filhos_por_dfs(out,n->l);
      escrever_filhos_por_dfs(out,n->r);
    }
  else
    {
      in.il = in.ir =  -1;
      fwrite(&in, 1, sizeof(in), out);
    }
}

/*!
  Nesse arranjo fica guardado o resultado da \ref codificação.

  São 256 - 1 = 255 nós internos no pior caso. Se para uma determinada folha
  todo nó interno formar o caminho da raíz até essa folha, então teremos 
  255 arestas nesse caminho. Logo o tamanho  de um código binário não ultrapassa 255 bits.
  Um caminho na árvore como o descrito acima é impossível de acontecer pois teríamos uma só folha
  na arvore, ou seja um só caractere no arquivo, o que seria representado por uma árvore de um só nó.
  Mesmo assim, o raciocínio utilizado serve para estipular, mesmo que grosseiramente, um limite superior
  para o tamanho de uma codificação.
  
  Colocaremos 300 bytes só por precaução.
 */
bool _bits[300];

/*! Computa a codificação do caractere \p c e guarda no arranjo \ref _bits em ordem inversa. Retorna 
  o número de bits da codificação.

  Assim, após uma chamada para <B>int n = codificacao(c)</B>
  o código associado ao caractere guardado em \p c será dado por <B>_bits[n - 1 ... 0]</B>. 
  Como exemplo, suponha que o caractere 'a' tenha código 01011001. Após <B>int n = codificacao('a')</B>,
  teremos n = 8 e _bits = {true,false,false,true,true,false,true,false, .... }
  
  O que o algoritmo faz é percorrer o caminho da folha associada ao caractere \p c até chegar na raiz,
  guardando em _bits[i],no caminho, o valor false caso o nó atual seja filho da esquerda e true caso contrário.
  No fim da execução <B>i</B> contém a quantidade de bits usados.
*/
int codificacao(uint8_t c)
{
  no * f = _folhas[(uint8_t)c];

  int i = 0;
  /* Termina quando f->p == NULL, ou seja, quando f for a raíz da árvore. */
  while(f->p)
    {
      /* O caminho para o filho da esquerda corresponde ao bit 0 enquanto o caminho para o
       da direita corresponde ao bit 1.*/
      if (f->p->l == f)
	_bits[i] = false;
      else
	_bits[i] = true;

      f = f->p;
      i++;
    }

  return i;
}

struct option long_options[] = 
{
  {"compress", required_argument, 0, 'c'},
  {"decompress", required_argument, 0, 'd'},
  {"output", required_argument, 0, 'o'},
  {"help",no_argument,0,'h'}
};


/*! Quando essa função é executada ela exibe informações sobre o programa e depois encerra a sua excução
 com uma chamada para exit(0). */
void uso(char * nome)
{
  puts("Thiago Teodoro Pereira Silva   (2018)\n");
  printf("Uso: %s [OPÇÂO] ...\n",nome);
  printf("-c, --compress nome_arquivo \t compacta o arquivo \"nome_arquivo\" \n");
  printf("-d, --decompress nome_arquivo \t descompacta o arquivo \"nome_arquivo\" \n");
  printf("-o, --output nome_saída \t Se o programa gerar um arquivo, ele o fará com o nome \"nome_saída\". O padrão é a.out\n");
  printf("-h, --help \t exibe essa ajuda \n");

  exit(0);
}

/*! Define as operações possíveis do programa. */
enum operation
  {
    NADA,
    COMPACTAR,
    DESCOMPACTAR
  };

/*! O coração do programa.  */
int main(int argc,char * argv[])
{
  enum operation op = NADA;


  char * in_name = NULL;
  char * out_name = NULL;
  for (int c; (c =
	       getopt_long
	       (argc, argv, "c:d:o:h", long_options,NULL)) != -1;)
    {
      switch (c)
	{
	case 'c':
	  in_name = optarg;
	  op = COMPACTAR;
	  break;
	case 'd':
	  in_name = optarg;
	  op = DESCOMPACTAR;
	  break;
	case 'o':
	  out_name = optarg;
	  break;
	case 'h':
	default:
	  uso(argv[0]);
	}
    }

  out_name = (out_name == NULL ? "a.out" : out_name);


  if (op == COMPACTAR)
    {
      FILE * in = fopen(in_name, "r");
      assert(in);

      /* Computa a frequência de cada caractere */
      for (int c; (c = fgetc(in)) != EOF;)
	_frequencias[(uint8_t)(c)]++;

      fseek(in,0,SEEK_SET);
  
      no * root  = huffman();

      FILE * out = fopen(out_name, "w");
      assert(out);


      /* Gravamos o tamanho da arvore no arquivo. */
      uint16_t tamanho_da_arvore = 2*_char_set_size - 1;
      fwrite(&tamanho_da_arvore, 1, sizeof(tamanho_da_arvore), out);
      /* Gravamos as informações de cada nó, conforme o modelo exposto na descrição desse arquivo fonte(main.c). */
      escrever_filhos_por_dfs(out,root);


      int c;

      /* Guarda a quantidade de bits escritos na variável byte. */
      uintmax_t byte_size = 0;
      uint8_t byte = 0;

      /* O while abaixo percorre todos os bytes do arquivo de entrada, gravando a codificação
	 no arquivo de saída sempre em múltiplos de 8 bits. Ou seja, pega 8 bits de codificação
	 e grava, depois mais 8 bits e grava novamente.
       */
      while ((c = fgetc(in)) != EOF)
	{
	  int size_bits = codificacao(c);

	  
	  for (int i = size_bits - 1; i >= 0; i--,byte_size++)
	    {
	      if (byte_size == 8)
		{
		  fputc(byte, out);
		  byte = 0;
		  byte_size = 0;
		}

	      if (_bits[i])
		byte |= 1 << (7 - byte_size);
	    }
	}


      /* O while acima sempre termina deixando conteúdo na variável byte para ser gravado. */
      fputc(byte, out);
      /* No final do arquivo nós escrevemos byte_size indicando a quantidade de 
       bits que deverá ser lido do último byte de codificação do arquivo.*/
      fprintf(out,"%c",(uint8_t)byte_size);
      
      fclose(out);
      free_tree(root);
      fclose(in);
    }
  else if (op == DESCOMPACTAR)
    {
      FILE * in = fopen(in_name,"r");
      assert(in);

      /* Calculamos o tamanho do arquivo e pegamos o seu último byte. Esse byte guarda a informação
	 de quantos bits devemos ler do penúltimo byte (que é o ultimo byte da codificação).
       */
      fseek(in, 0, SEEK_END);

      long in_size = ftell(in);

      fseek(in,in_size - 1,SEEK_SET);
      uint8_t quantos_bits_ultimo_byte;
      fscanf(in,"%c",&quantos_bits_ultimo_byte);

      fseek(in,0,SEEK_SET);
      
      FILE * out = fopen(out_name,"w");
      assert(out);

      /* N guardará a quantidade de nós da árvore binária de codificação. */
      uint16_t N;
      fread(&N, 1, sizeof(N), in);

      no * no_array = (no *)malloc(sizeof(no)*N);
      assert(no_array);

      /* Montamos a árvore binária de codificação. */
      no * root = NULL;
      for (int i = 0; i < N; i++)
	{
	  info_no noi;
	  fread(&noi, 1, sizeof(noi), in);


	  /* A primeira linha sempre descreve a raiz da árvore. */
	  if (i == 0)
	    root = &no_array[noi.indice];

	  /* Não vamos preencher os outros campos pois eles não serão necessários para 
	     essa implementação da função de descompactação.
	  */
	  no_array[noi.indice].c = noi.c;
	  no_array[noi.indice].l = ((int16_t)noi.il == -1 ? NULL : &no_array[noi.il]);
	  no_array[noi.indice].r = ((int16_t)noi.ir == -1 ? NULL : &no_array[noi.ir]);
	}


      /* Guarda a posição do próximo bit que iremos ler no byte atual. */
      uintmax_t bit_pos = 0;
      uint8_t byte;
      /* Esse loop é o responsável por decodificar a informação do arquivo de entrada.
	 Ele vai processando os bits sempre em multiplos de 8. Assim a cada 8 bits pegamos
	 o próximo byte da entrada.
       */

      for (;;)
	{	
	  no * f = root;

	  /* Se já leu todos os bits damos um break.  */
	  if (ftell(in) == in_size - 1 &&
	      bit_pos >= quantos_bits_ultimo_byte)
	    break;

	  /* Percorremos a arvore até encontramos o nó correspondente ao caractere da codificação
	   processada. Depois do loop terminar, f guardará o ponteiro para esse nó, bastando gravar
	  o caractere f->c no arquivo de saída.*/
	  while (f->l)
	    {
	      if (bit_pos == 8 || bit_pos == 0)
		{
		  int c = fgetc(in);
		  if (c == EOF)
		    goto fim;

		  byte = c;
		  bit_pos = 0;
		}


	      /* Vamos para a direita */
	      if (byte & (1 << (7 - bit_pos)))
		f = f->r;
	      else 			/* Vamos para a esquerda */
		f = f->l;

	      bit_pos++;
	    }

	  fputc(f->c,out);
	}      

    fim:
      free(no_array);
      fclose(in);
      fclose(out);
    }
  else
    {
      printf("Você precisa especificar pelo menos uma das opções -[cd].\n");
      printf("Execute \"%s --help\" para obter ajuda.\n",argv[0]);
    }

  return 0;
}


