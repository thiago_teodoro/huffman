/*!
  \file heap.h
  \brief Esse é o cabeçalho da fila de prioridade mínima.

  Aqui estão declarados todos os métodos necessários para se 
  utilizar a fila de prioridade mínima. Essa fila foi implementada
  de forma a ser flexível, permitindo o uso de diversos tipos de dados,
  sendo eles selecionados através da macro \ref HEAP_TYPE. Por simplicidade,
  foi colocada a definição da struct \ref no nesse arquivo. Para utilizar 
  essa fila de prioridade mínima em outro projeto basta remover as definições
  associadas a essa struct e definir a macro HEAP_TYPE a seu gosto. Defina 
  ela como void * caso você deseje utilizar várias filas de prioridades 
  com tipos de dados distintos.
 */

#ifndef HEAP_H
#define HEAP_H

#include <stdbool.h>
#include <inttypes.h>

typedef struct no no;

/*! A estrutura que representa os nós na árvore de huffman. */
struct no
{
  uint8_t c; 			/*!< O caractere que o no representa  */
  int freq;			/*!< A frequência do nó de acordo com a execução 
				  do algoritmo de huffman  */
  no * l,*r,*p;			/*!< O ponteiro para o nó da esquerda,
				  direita e para o seu pai,respectivamente.  */
  int indice;			/*!< O índice do nó. É utilizado para permitir a 
				  enumeração dos nós na arvore de huffman.   */
};


/*!
  \brief O tipo de dados que o heap irá guardar.

  A fila de prioridade implementada nos arquivos heap.h e heap.c foi pensada 
  de forma a aceitar qualquer tipo de dados.
  */

#define HEAP_TYPE no*

typedef struct heap heap;



/*!
  \brief Aloca um novo heap. O resultado dessa alocação deve ser passado para a função \ref free_heap.

  \param[in] l Ponteiro para a função que dados \p a e \p b retorna verdadeiro caso \p a seja menor do que \p b 
  e falso caso contrário.
  */
heap * new_heap(bool (*l)(HEAP_TYPE a,HEAP_TYPE b));

/*!
  \brief Desaloca o heap \p h.
*/
void free_heap(heap * h);

/*!
 \brief Insere a chave \p key no \p heap apontado por h.
*/
void min_heap_insert(heap * h, HEAP_TYPE key);

/*!
  \brief Remove e retorna o menor elemento do heap \p h.
*/
HEAP_TYPE heap_extract_min(heap * h);

/*!
  \brief Retorna a quantidade de elementos guardados no heap apontado por \p h.
*/
int heap_size(heap * h);



#endif	/* HEAP_H */


