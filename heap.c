/*!
  \file heap.c
  \brief Arquivo onde são implementadas todas as funções descritas em \r heap.h .
  */

#include <inttypes.h>
#include <assert.h>
#include <stdlib.h>
#include "heap.h"

/*!
  \brief A estrutura utilizada para implementar a estrutura de dados heap.
*/
struct heap
{
  HEAP_TYPE * array; 		/*<! O arranjo utilizado para guardas as chaves.  */
  int heap_size;		/*<! A variável que guarda o tamanho do heap.  */
  bool (*less_than)(HEAP_TYPE a,HEAP_TYPE b); /*<! A relação de ordem entre os elementos do heap.  */
};


/* Provar que isso funciona */
#define parent(j) (((j) - 1)/2) /*<! Retorna o pai do nó j.  */
#define left(j) (2*(j) + 1)     /*<! Retorna o filho da esquerda do nó j */
#define righ(j) (left(j) + 1)   /*<! Retorna o filho da direita  do nó j */






/*!
  Não há nada muito especial para ser dito aqui, a função simplesmente
  aloca um novo heap e preenche os seus parâmetros internos.
*/
heap * new_heap(bool (*l)(HEAP_TYPE a,HEAP_TYPE b))
{
  heap * h = (heap *)malloc(sizeof(heap));
  assert(h);

  h->array = NULL;
  h->heap_size = 0;
  h->less_than = l;
  
  return h;
}
/*!
  Desaloca o membro array do heap h e depois desaloca o próprio heap. 
*/
void free_heap(heap * h)
{
  if (h->array)
    free(h->array);

  free(h);
}

/*! A documentação segue dentro do corpo da função.  */
void min_heap_insert(heap * h, HEAP_TYPE key)
{
  assert(h);

  /* Aumentamos o tamanho do arranjo guardado pelo heap h em uma unidade.
     É bem verdade que esse não é um jeito eficiente de fazer o aumento no tamanho do arranjo.
     Algo mais otimizado seria alocar memória extra, mas eu fiz assim por simplicidade.
   */
  h->array = realloc(h->array,(h->heap_size + 1)*sizeof(HEAP_TYPE));
  assert(h->array);

  h->heap_size++;

  HEAP_TYPE * A = h->array;

  /* Insere a chave key no final do heap. */
  A[h->heap_size - 1] = key;

  /* A ideia aqui é ir percorrendo a árvore de baixo para cima em busca da posição
     adequada para guardar o nó. Para mais informações consulte o livro de Algoritmos e Estruturas
     de Dados do Cormen no capítulo sobre fila de prioridade mínima.
  */
  int i = h->heap_size - 1;
  while (i > 0 &&  h->less_than(A[i],A[parent(i)]))
    {
      HEAP_TYPE b = A[parent(i)];
      A[parent(i)] = A[i];
      A[i] = b;

      i = parent(i);
    }
}

/*! Essa função recebe um nó i do heap h, onde as subárvores enraizadas pelos seus filhos 
   são heaps de mínimo. O que ela faz é rearranjar o nó i,left(i) e right(i) de forma que 
   esses três nós satisfaçam a propriedade de heap de mínimo. Se eles já satisfazem essa propriedade,
   então não fazemos alterações, caso contrário, após o rearranjo dos nós, chamamos a função recursivamente
   no filho que teve o seu valor alterado, de forma a garantir que a subárvore enraizada por ele seja um 
   heap de mínimo. Para mais detalhes consulte o livro Algoritmos e Estruturas de Dados do Cormen .
  */
void min_heapify(heap * h,int i)
{
  HEAP_TYPE * A = h->array;
  int heap_size = h->heap_size;

  int min;

  int l = left(i);
  int r = righ(i);
  
  if (r < heap_size && h->less_than(A[r],A[i]))
    min = r;
  else
    min = i;

  if (l < heap_size && h->less_than(A[l],A[min]))
    min = l;

  if (min != i)
    {
      HEAP_TYPE b = A[i];
      A[i] = A[min];
      A[min] = b;

      min_heapify(h, min);
    }
}

/*! Nós pegamos o último elemento do heap e o colocamos na raiz e diminuímos o tamanho do heap em uma unidade.
   Em seguida chamamos o min_heapify nesse nó a fim de garantir a propriedade de heap de mínimo.
*/
HEAP_TYPE heap_extract_min(heap * h)
{
  assert(h->heap_size > 0);

  HEAP_TYPE * A = h->array;

  HEAP_TYPE r = A[0];

  A[0] = A[h->heap_size - 1];
  h->heap_size--;

  min_heapify(h, 0);

  return r;
}

/*! O código nem precisa ser comentado. */
int heap_size(heap * h)
{
  return h->heap_size;
}
