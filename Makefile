GCC = gcc  -Wall -Wextra -O0 -g3 

huffman: main.c heap.c
	${GCC} $^ -o $@
doc: main.c heap.c
	doxygen Doxyfile
